package com.avery.staging.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(name = "props", value = "classpath:dify.properties")
public class DifyConfig {

}
