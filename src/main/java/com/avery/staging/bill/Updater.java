package com.avery.staging.bill;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.avery.staging.config.DifyConfig;
import com.avery.staging.csv.CsvSalesOrderItemReader;
import com.avery.staging.csv.CsvSalesOrderReader;
import com.avery.staging.dao.SalesFlatOrderDao;
import com.avery.staging.dao.SalesFlatOrderItemDao;
import com.avery.staging.model.SalesFlatOrder;
import com.avery.staging.model.SalesFlatOrderItem;


public class Updater{

	
	protected ApplicationContext ctx;
	private   Properties props;
    protected static List<SalesFlatOrder> orderList;
    protected static List<SalesFlatOrderItem> orderItemList;
    
    private static final Logger logger = Logger.getLogger(Updater.class);
	
	public static void main(String[] args) {
		
        logger.info("######################");
    
        Updater updater = new Updater();
        updater.initProperties();
        updater.initSpring();
       
        //orderList = updater.readSalesOrderCsvFiles();
        //updater.populateInvoice(orderList);
        
        orderItemList = updater.readSalesOrderItemCsvFiles();
        updater.populateSalesOrderItem(orderItemList);
	}
	
	protected List<SalesFlatOrder> readSalesOrderCsvFiles(){
		
	   CsvSalesOrderReader csv = new CsvSalesOrderReader();
	   try {
		    
		     orderList = csv.readCsv(props.getProperty("order_file_path"));
	   } catch (IOException e) {
		     logger.error(e.getMessage());
	   }
	
	   return orderList;
	   
	}
	
	protected List<SalesFlatOrderItem> readSalesOrderItemCsvFiles(){
		
		   CsvSalesOrderItemReader csv = new CsvSalesOrderItemReader();
		   try {
			    
			     orderItemList = csv.readCsv(props.getProperty("order_item_file_path"));
		   } catch (IOException e) {
			     logger.error(e.getMessage());
		   }
		
		   return orderItemList;
		   
		}
	
	protected void populateInvoice(List<SalesFlatOrder> orderList){
		
		 SalesFlatOrderDao dao = ctx.getBean(SalesFlatOrderDao.class);
		 for(SalesFlatOrder flatOrder : orderList){
			 dao.updateRecord(flatOrder);
	        }
	}
	
	protected void populateSalesOrderItem(List<SalesFlatOrderItem> orderItemList){
		
		 SalesFlatOrderItemDao dao = ctx.getBean(SalesFlatOrderItemDao.class);
		 for(SalesFlatOrderItem flatOrderItem : orderItemList){
			 dao.updateRecord(flatOrderItem);
	        }
	}
	
	
	protected void initSpring(){
		ctx = new ClassPathXmlApplicationContext("spring-config.xml");
	}

	protected void initProperties() {
		try {
			InputStream is = Updater.class.getClassLoader()
					.getResourceAsStream("dify.properties");
			props = new Properties();
			props.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
