package com.avery.staging.model;

import java.math.BigDecimal;
import java.util.Date;

public class SalesFlatOrderItem {
	
	Integer 	 ITEM_ID;     
	Integer 	 ORDER_ID;     
	Integer 	 PARENT_ITEM_ID;     
	Integer 	 QUOTE_ITEM_ID;     
	Integer 	 STORE_ID;
	Date    	 CREATED_AT;
	Date    	 UPDATED_AT;
	Integer 	 PRODUCT_ID;
	String  	 PRODUCT_TYPE;  
	String  	 PRODUCT_OPTIONS; 
	BigDecimal   WEIGHT;   
	Integer      IS_VIRTUAL;     
	String       SKU;  
	String       NAME;             
	String       DESCRIPTION; 
	String       APPLIED_RULE_IDS; 
	String       ADDITIONAL_DATA; 
	Integer      FREE_SHIPPING;     
	Integer      IS_QTY_DECIMAL;     
	Integer      NO_DISCOUNT;     
	BigDecimal   QTY_BACKORDERED;   
	BigDecimal   QTY_CANCELED;   
	BigDecimal   QTY_INVOICED;                            
	BigDecimal   QTY_ORDERED;                             
	BigDecimal   QTY_REFUNDED;                             
	BigDecimal   QTY_SHIPPED;                             
	BigDecimal   BASE_COST;                                 
	BigDecimal   PRICE;                            
	BigDecimal   BASE_PRICE;                      
	BigDecimal   ORIGINAL_PRICE;                         
	BigDecimal   BASE_ORIGINAL_PRICE;                      
	BigDecimal   TAX_PERCENT;                          
	BigDecimal   TAX_AMOUNT;                               
	BigDecimal   BASE_TAX_AMOUNT;                           
	BigDecimal   TAX_INVOICED;                              
	BigDecimal   BASE_TAX_INVOICED;   
	BigDecimal   DISCOUNT_PERCENT;   
	BigDecimal   DISCOUNT_AMOUNT;   
	BigDecimal   BASE_DISCOUNT_AMOUNT;   
	BigDecimal   DISCOUNT_INVOICED;   
	BigDecimal   BASE_DISCOUNT_INVOICED;   
	BigDecimal   AMOUNT_REFUNDED;
	BigDecimal   BASE_AMOUNT_REFUNDED;   
	BigDecimal   ROW_TOTAL;   
	BigDecimal   BASE_ROW_TOTAL;   
	BigDecimal   ROW_INVOICED;   
	BigDecimal   BASE_ROW_INVOICED;   
	BigDecimal   ROW_WEIGHT;   
	BigDecimal   BASE_TAX_BEFORE_DISCOUNT;
	BigDecimal   TAX_BEFORE_DISCOUNT;   
	String       EXT_ORDER_ITEM_ID;  
	Integer      LOCKED_DO_INVOICE;     
	Integer      LOCKED_DO_SHIP;
	BigDecimal   PRICE_INCL_TAX;   
	BigDecimal   BASE_PRICE_INCL_TAX;   
	BigDecimal   ROW_TOTAL_INCL_TAX;   
	BigDecimal   BASE_ROW_TOTAL_INCL_TAX;   
	BigDecimal   HIDDEN_TAX_AMOUNT;   
	BigDecimal   BASE_HIDDEN_TAX_AMOUNT;   
	BigDecimal   HIDDEN_TAX_INVOICED;   
	BigDecimal   BASE_HIDDEN_TAX_INVOICED;   
	BigDecimal   HIDDEN_TAX_REFUNDED;   
	BigDecimal   BASE_HIDDEN_TAX_REFUNDED;   
	Integer      IS_NOMINAL;     
	BigDecimal   TAX_CANCELED;   
	BigDecimal   HIDDEN_TAX_CANCELED;   
	BigDecimal   TAX_REFUNDED;   
	BigDecimal   BASE_TAX_REFUNDED;   
	BigDecimal   DISCOUNT_REFUNDED;   
	BigDecimal   BASE_DISCOUNT_REFUNDED;   
	Integer      GIFT_MESSAGE_ID;     
	Integer      GIFT_MESSAGE_AVAILABLE;     
	String       WEEE_TAX_APPLIED; 
	BigDecimal   WEEE_TAX_APPLIED_AMOUNT;   
	BigDecimal   WEEE_TAX_APPLIED_ROW_AMOUNT;   
	BigDecimal   BASE_WEEE_TAX_APPLIED_AMOUNT;   
	BigDecimal   BASE_WEEE_TAX_APPLIED_ROW_AMNT;   
	BigDecimal   WEEE_TAX_DISPOSITION;
	BigDecimal   WEEE_TAX_ROW_DISPOSITION;
	BigDecimal   BASE_WEEE_TAX_DISPOSITION;
	BigDecimal   BASE_WEEE_TAX_ROW_DISPOSITION;   
	Integer      EVENT_ID;     
	Integer      GIFTREGISTRY_ITEM_ID;     
	Integer      GW_ID;     
	BigDecimal   GW_BASE_PRICE;   
	BigDecimal   GW_PRICE;   
	BigDecimal   GW_BASE_TAX_AMOUNT; 
	BigDecimal   GW_TAX_AMOUNT;
	BigDecimal   GW_BASE_PRICE_INVOICED;
	BigDecimal   GW_PRICE_INVOICED;
	BigDecimal   GW_BASE_TAX_AMOUNT_INVOICED;
	BigDecimal   GW_TAX_AMOUNT_INVOICED;
	BigDecimal   GW_BASE_PRICE_REFUNDED;  
	BigDecimal   GW_PRICE_REFUNDED;  
	BigDecimal   GW_BASE_TAX_AMOUNT_REFUNDED;  
	BigDecimal   GW_TAX_AMOUNT_REFUNDED;
	BigDecimal   QTY_RETURNED;
	
	public SalesFlatOrderItem(){
		
	}
	

	@Override
	public String toString() {
		return "SalesFlatOrderItem [ITEM_ID=" + ITEM_ID + ", ORDER_ID="
				+ ORDER_ID + ", PARENT_ITEM_ID=" + PARENT_ITEM_ID
				+ ", QUOTE_ITEM_ID=" + QUOTE_ITEM_ID + ", STORE_ID=" + STORE_ID
				+ ", CREATED_AT=" + CREATED_AT + ", UPDATED_AT=" + UPDATED_AT
				+ ", PRODUCT_ID=" + PRODUCT_ID + ", PRODUCT_TYPE="
				+ PRODUCT_TYPE + ", PRODUCT_OPTIONS=" + PRODUCT_OPTIONS
				+ ", WEIGHT=" + WEIGHT + ", IS_VIRTUAL=" + IS_VIRTUAL
				+ ", SKU=" + SKU + ", NAME=" + NAME + ", DESCRIPTION="
				+ DESCRIPTION + ", APPLIED_RULE_IDS=" + APPLIED_RULE_IDS
				+ ", ADDITIONAL_DATA=" + ADDITIONAL_DATA + ", FREE_SHIPPING="
				+ FREE_SHIPPING + ", IS_QTY_DECIMAL=" + IS_QTY_DECIMAL
				+ ", NO_DISCOUNT=" + NO_DISCOUNT + ", QTY_BACKORDERED="
				+ QTY_BACKORDERED + ", QTY_CANCELED=" + QTY_CANCELED
				+ ", QTY_INVOICED=" + QTY_INVOICED + ", QTY_ORDERED="
				+ QTY_ORDERED + ", QTY_REFUNDED=" + QTY_REFUNDED
				+ ", QTY_SHIPPED=" + QTY_SHIPPED + ", BASE_COST=" + BASE_COST
				+ ", PRICE=" + PRICE + ", BASE_PRICE=" + BASE_PRICE
				+ ", ORIGINAL_PRICE=" + ORIGINAL_PRICE
				+ ", BASE_ORIGINAL_PRICE=" + BASE_ORIGINAL_PRICE
				+ ", TAX_PERCENT=" + TAX_PERCENT + ", TAX_AMOUNT=" + TAX_AMOUNT
				+ ", BASE_TAX_AMOUNT=" + BASE_TAX_AMOUNT + ", TAX_INVOICED="
				+ TAX_INVOICED + ", BASE_TAX_INVOICED=" + BASE_TAX_INVOICED
				+ ", DISCOUNT_PERCENT=" + DISCOUNT_PERCENT
				+ ", DISCOUNT_AMOUNT=" + DISCOUNT_AMOUNT
				+ ", BASE_DISCOUNT_AMOUNT=" + BASE_DISCOUNT_AMOUNT
				+ ", DISCOUNT_INVOICED=" + DISCOUNT_INVOICED
				+ ", BASE_DISCOUNT_INVOICED=" + BASE_DISCOUNT_INVOICED
				+ ", AMOUNT_REFUNDED=" + AMOUNT_REFUNDED
				+ ", BASE_AMOUNT_REFUNDED=" + BASE_AMOUNT_REFUNDED
				+ ", ROW_TOTAL=" + ROW_TOTAL + ", BASE_ROW_TOTAL="
				+ BASE_ROW_TOTAL + ", ROW_INVOICED=" + ROW_INVOICED
				+ ", BASE_ROW_INVOICED=" + BASE_ROW_INVOICED + ", ROW_WEIGHT="
				+ ROW_WEIGHT + ", BASE_TAX_BEFORE_DISCOUNT="
				+ BASE_TAX_BEFORE_DISCOUNT + ", TAX_BEFORE_DISCOUNT="
				+ TAX_BEFORE_DISCOUNT + ", EXT_ORDER_ITEM_ID="
				+ EXT_ORDER_ITEM_ID + ", LOCKED_DO_INVOICE="
				+ LOCKED_DO_INVOICE + ", LOCKED_DO_SHIP=" + LOCKED_DO_SHIP
				+ ", PRICE_INCL_TAX=" + PRICE_INCL_TAX
				+ ", BASE_PRICE_INCL_TAX=" + BASE_PRICE_INCL_TAX
				+ ", ROW_TOTAL_INCL_TAX=" + ROW_TOTAL_INCL_TAX
				+ ", BASE_ROW_TOTAL_INCL_TAX=" + BASE_ROW_TOTAL_INCL_TAX
				+ ", HIDDEN_TAX_AMOUNT=" + HIDDEN_TAX_AMOUNT
				+ ", BASE_HIDDEN_TAX_AMOUNT=" + BASE_HIDDEN_TAX_AMOUNT
				+ ", HIDDEN_TAX_INVOICED=" + HIDDEN_TAX_INVOICED
				+ ", BASE_HIDDEN_TAX_INVOICED=" + BASE_HIDDEN_TAX_INVOICED
				+ ", HIDDEN_TAX_REFUNDED=" + HIDDEN_TAX_REFUNDED
				+ ", BASE_HIDDEN_TAX_REFUNDED=" + BASE_HIDDEN_TAX_REFUNDED
				+ ", IS_NOMINAL=" + IS_NOMINAL + ", TAX_CANCELED="
				+ TAX_CANCELED + ", HIDDEN_TAX_CANCELED=" + HIDDEN_TAX_CANCELED
				+ ", TAX_REFUNDED=" + TAX_REFUNDED + ", BASE_TAX_REFUNDED="
				+ BASE_TAX_REFUNDED + ", DISCOUNT_REFUNDED="
				+ DISCOUNT_REFUNDED + ", BASE_DISCOUNT_REFUNDED="
				+ BASE_DISCOUNT_REFUNDED + ", GIFT_MESSAGE_ID="
				+ GIFT_MESSAGE_ID + ", GIFT_MESSAGE_AVAILABLE="
				+ GIFT_MESSAGE_AVAILABLE + ", WEEE_TAX_APPLIED="
				+ WEEE_TAX_APPLIED + ", WEEE_TAX_APPLIED_AMOUNT="
				+ WEEE_TAX_APPLIED_AMOUNT + ", WEEE_TAX_APPLIED_ROW_AMOUNT="
				+ WEEE_TAX_APPLIED_ROW_AMOUNT
				+ ", BASE_WEEE_TAX_APPLIED_AMOUNT="
				+ BASE_WEEE_TAX_APPLIED_AMOUNT
				+ ", BASE_WEEE_TAX_APPLIED_ROW_AMNT="
				+ BASE_WEEE_TAX_APPLIED_ROW_AMNT + ", WEEE_TAX_DISPOSITION="
				+ WEEE_TAX_DISPOSITION + ", WEEE_TAX_ROW_DISPOSITION="
				+ WEEE_TAX_ROW_DISPOSITION + ", BASE_WEEE_TAX_DISPOSITION="
				+ BASE_WEEE_TAX_DISPOSITION
				+ ", BASE_WEEE_TAX_ROW_DISPOSITION="
				+ BASE_WEEE_TAX_ROW_DISPOSITION + ", EVENT_ID=" + EVENT_ID
				+ ", GIFTREGISTRY_ITEM_ID=" + GIFTREGISTRY_ITEM_ID + ", GW_ID="
				+ GW_ID + ", GW_BASE_PRICE=" + GW_BASE_PRICE + ", GW_PRICE="
				+ GW_PRICE + ", GW_BASE_TAX_AMOUNT=" + GW_BASE_TAX_AMOUNT
				+ ", GW_TAX_AMOUNT=" + GW_TAX_AMOUNT
				+ ", GW_BASE_PRICE_INVOICED=" + GW_BASE_PRICE_INVOICED
				+ ", GW_PRICE_INVOICED=" + GW_PRICE_INVOICED
				+ ", GW_BASE_TAX_AMOUNT_INVOICED="
				+ GW_BASE_TAX_AMOUNT_INVOICED + ", GW_TAX_AMOUNT_INVOICED="
				+ GW_TAX_AMOUNT_INVOICED + ", GW_BASE_PRICE_REFUNDED="
				+ GW_BASE_PRICE_REFUNDED + ", GW_PRICE_REFUNDED="
				+ GW_PRICE_REFUNDED + ", GW_BASE_TAX_AMOUNT_REFUNDED="
				+ GW_BASE_TAX_AMOUNT_REFUNDED + ", GW_TAX_AMOUNT_REFUNDED="
				+ GW_TAX_AMOUNT_REFUNDED + ", QTY_RETURNED=" + QTY_RETURNED
				+ "]";
	}

	public Integer getITEM_ID() {
		return ITEM_ID;
	}

	public void setITEM_ID(Integer iTEM_ID) {
		ITEM_ID = iTEM_ID;
	}

	public Integer getORDER_ID() {
		return ORDER_ID;
	}

	public void setORDER_ID(Integer oRDER_ID) {
		ORDER_ID = oRDER_ID;
	}

	public Integer getPARENT_ITEM_ID() {
		return PARENT_ITEM_ID;
	}

	public void setPARENT_ITEM_ID(Integer pARENT_ITEM_ID) {
		PARENT_ITEM_ID = pARENT_ITEM_ID;
	}

	public Integer getQUOTE_ITEM_ID() {
		return QUOTE_ITEM_ID;
	}

	public void setQUOTE_ITEM_ID(Integer qUOTE_ITEM_ID) {
		QUOTE_ITEM_ID = qUOTE_ITEM_ID;
	}

	public Integer getSTORE_ID() {
		return STORE_ID;
	}

	public void setSTORE_ID(Integer sTORE_ID) {
		STORE_ID = sTORE_ID;
	}

	public Date getCREATED_AT() {
		return CREATED_AT;
	}

	public void setCREATED_AT(Date cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}

	public Date getUPDATED_AT() {
		return UPDATED_AT;
	}

	public void setUPDATED_AT(Date uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}

	public Integer getPRODUCT_ID() {
		return PRODUCT_ID;
	}

	public void setPRODUCT_ID(Integer pRODUCT_ID) {
		PRODUCT_ID = pRODUCT_ID;
	}

	public String getPRODUCT_TYPE() {
		return PRODUCT_TYPE;
	}

	public void setPRODUCT_TYPE(String pRODUCT_TYPE) {
		PRODUCT_TYPE = pRODUCT_TYPE;
	}

	public String getPRODUCT_OPTIONS() {
		return PRODUCT_OPTIONS;
	}

	public void setPRODUCT_OPTIONS(String pRODUCT_OPTIONS) {
		PRODUCT_OPTIONS = pRODUCT_OPTIONS;
	}

	public BigDecimal getWEIGHT() {
		return WEIGHT;
	}

	public void setWEIGHT(BigDecimal wEIGHT) {
		WEIGHT = wEIGHT;
	}

	public Integer getIS_VIRTUAL() {
		return IS_VIRTUAL;
	}

	public void setIS_VIRTUAL(Integer iS_VIRTUAL) {
		IS_VIRTUAL = iS_VIRTUAL;
	}

	public String getSKU() {
		return SKU;
	}

	public void setSKU(String sKU) {
		SKU = sKU;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getDESCRIPTION() {
		return DESCRIPTION;
	}

	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}

	public String getAPPLIED_RULE_IDS() {
		return APPLIED_RULE_IDS;
	}

	public void setAPPLIED_RULE_IDS(String aPPLIED_RULE_IDS) {
		APPLIED_RULE_IDS = aPPLIED_RULE_IDS;
	}

	public String getADDITIONAL_DATA() {
		return ADDITIONAL_DATA;
	}

	public void setADDITIONAL_DATA(String aDDITIONAL_DATA) {
		ADDITIONAL_DATA = aDDITIONAL_DATA;
	}

	public Integer getFREE_SHIPPING() {
		return FREE_SHIPPING;
	}

	public void setFREE_SHIPPING(Integer fREE_SHIPPING) {
		FREE_SHIPPING = fREE_SHIPPING;
	}

	public Integer getIS_QTY_DECIMAL() {
		return IS_QTY_DECIMAL;
	}

	public void setIS_QTY_DECIMAL(Integer iS_QTY_DECIMAL) {
		IS_QTY_DECIMAL = iS_QTY_DECIMAL;
	}

	public Integer getNO_DISCOUNT() {
		return NO_DISCOUNT;
	}

	public void setNO_DISCOUNT(Integer nO_DISCOUNT) {
		NO_DISCOUNT = nO_DISCOUNT;
	}

	public BigDecimal getQTY_BACKORDERED() {
		return QTY_BACKORDERED;
	}

	public void setQTY_BACKORDERED(BigDecimal qTY_BACKORDERED) {
		QTY_BACKORDERED = qTY_BACKORDERED;
	}

	public BigDecimal getQTY_CANCELED() {
		return QTY_CANCELED;
	}

	public void setQTY_CANCELED(BigDecimal qTY_CANCELED) {
		QTY_CANCELED = qTY_CANCELED;
	}

	public BigDecimal getQTY_INVOICED() {
		return QTY_INVOICED;
	}

	public void setQTY_INVOICED(BigDecimal qTY_INVOICED) {
		QTY_INVOICED = qTY_INVOICED;
	}

	public BigDecimal getQTY_ORDERED() {
		return QTY_ORDERED;
	}

	public void setQTY_ORDERED(BigDecimal qTY_ORDERED) {
		QTY_ORDERED = qTY_ORDERED;
	}

	public BigDecimal getQTY_REFUNDED() {
		return QTY_REFUNDED;
	}

	public void setQTY_REFUNDED(BigDecimal qTY_REFUNDED) {
		QTY_REFUNDED = qTY_REFUNDED;
	}

	public BigDecimal getQTY_SHIPPED() {
		return QTY_SHIPPED;
	}

	public void setQTY_SHIPPED(BigDecimal qTY_SHIPPED) {
		QTY_SHIPPED = qTY_SHIPPED;
	}

	public BigDecimal getBASE_COST() {
		return BASE_COST;
	}

	public void setBASE_COST(BigDecimal bASE_COST) {
		BASE_COST = bASE_COST;
	}

	public BigDecimal getPRICE() {
		return PRICE;
	}

	public void setPRICE(BigDecimal pRICE) {
		PRICE = pRICE;
	}

	public BigDecimal getBASE_PRICE() {
		return BASE_PRICE;
	}

	public void setBASE_PRICE(BigDecimal bASE_PRICE) {
		BASE_PRICE = bASE_PRICE;
	}

	public BigDecimal getORIGINAL_PRICE() {
		return ORIGINAL_PRICE;
	}

	public void setORIGINAL_PRICE(BigDecimal oRIGINAL_PRICE) {
		ORIGINAL_PRICE = oRIGINAL_PRICE;
	}

	public BigDecimal getBASE_ORIGINAL_PRICE() {
		return BASE_ORIGINAL_PRICE;
	}

	public void setBASE_ORIGINAL_PRICE(BigDecimal bASE_ORIGINAL_PRICE) {
		BASE_ORIGINAL_PRICE = bASE_ORIGINAL_PRICE;
	}

	public BigDecimal getTAX_PERCENT() {
		return TAX_PERCENT;
	}

	public void setTAX_PERCENT(BigDecimal tAX_PERCENT) {
		TAX_PERCENT = tAX_PERCENT;
	}

	public BigDecimal getTAX_AMOUNT() {
		return TAX_AMOUNT;
	}

	public void setTAX_AMOUNT(BigDecimal tAX_AMOUNT) {
		TAX_AMOUNT = tAX_AMOUNT;
	}

	public BigDecimal getBASE_TAX_AMOUNT() {
		return BASE_TAX_AMOUNT;
	}

	public void setBASE_TAX_AMOUNT(BigDecimal bASE_TAX_AMOUNT) {
		BASE_TAX_AMOUNT = bASE_TAX_AMOUNT;
	}

	public BigDecimal getTAX_INVOICED() {
		return TAX_INVOICED;
	}

	public void setTAX_INVOICED(BigDecimal tAX_INVOICED) {
		TAX_INVOICED = tAX_INVOICED;
	}

	public BigDecimal getBASE_TAX_INVOICED() {
		return BASE_TAX_INVOICED;
	}

	public void setBASE_TAX_INVOICED(BigDecimal bASE_TAX_INVOICED) {
		BASE_TAX_INVOICED = bASE_TAX_INVOICED;
	}

	public BigDecimal getDISCOUNT_PERCENT() {
		return DISCOUNT_PERCENT;
	}

	public void setDISCOUNT_PERCENT(BigDecimal dISCOUNT_PERCENT) {
		DISCOUNT_PERCENT = dISCOUNT_PERCENT;
	}

	public BigDecimal getDISCOUNT_AMOUNT() {
		return DISCOUNT_AMOUNT;
	}

	public void setDISCOUNT_AMOUNT(BigDecimal dISCOUNT_AMOUNT) {
		DISCOUNT_AMOUNT = dISCOUNT_AMOUNT;
	}

	public BigDecimal getBASE_DISCOUNT_AMOUNT() {
		return BASE_DISCOUNT_AMOUNT;
	}

	public void setBASE_DISCOUNT_AMOUNT(BigDecimal bASE_DISCOUNT_AMOUNT) {
		BASE_DISCOUNT_AMOUNT = bASE_DISCOUNT_AMOUNT;
	}

	public BigDecimal getDISCOUNT_INVOICED() {
		return DISCOUNT_INVOICED;
	}

	public void setDISCOUNT_INVOICED(BigDecimal dISCOUNT_INVOICED) {
		DISCOUNT_INVOICED = dISCOUNT_INVOICED;
	}

	public BigDecimal getBASE_DISCOUNT_INVOICED() {
		return BASE_DISCOUNT_INVOICED;
	}

	public void setBASE_DISCOUNT_INVOICED(BigDecimal bASE_DISCOUNT_INVOICED) {
		BASE_DISCOUNT_INVOICED = bASE_DISCOUNT_INVOICED;
	}

	public BigDecimal getAMOUNT_REFUNDED() {
		return AMOUNT_REFUNDED;
	}

	public void setAMOUNT_REFUNDED(BigDecimal aMOUNT_REFUNDED) {
		AMOUNT_REFUNDED = aMOUNT_REFUNDED;
	}

	public BigDecimal getBASE_AMOUNT_REFUNDED() {
		return BASE_AMOUNT_REFUNDED;
	}

	public void setBASE_AMOUNT_REFUNDED(BigDecimal bASE_AMOUNT_REFUNDED) {
		BASE_AMOUNT_REFUNDED = bASE_AMOUNT_REFUNDED;
	}

	public BigDecimal getROW_TOTAL() {
		return ROW_TOTAL;
	}

	public void setROW_TOTAL(BigDecimal rOW_TOTAL) {
		ROW_TOTAL = rOW_TOTAL;
	}

	public BigDecimal getBASE_ROW_TOTAL() {
		return BASE_ROW_TOTAL;
	}

	public void setBASE_ROW_TOTAL(BigDecimal bASE_ROW_TOTAL) {
		BASE_ROW_TOTAL = bASE_ROW_TOTAL;
	}

	public BigDecimal getROW_INVOICED() {
		return ROW_INVOICED;
	}

	public void setROW_INVOICED(BigDecimal rOW_INVOICED) {
		ROW_INVOICED = rOW_INVOICED;
	}

	public BigDecimal getBASE_ROW_INVOICED() {
		return BASE_ROW_INVOICED;
	}

	public void setBASE_ROW_INVOICED(BigDecimal bASE_ROW_INVOICED) {
		BASE_ROW_INVOICED = bASE_ROW_INVOICED;
	}

	public BigDecimal getROW_WEIGHT() {
		return ROW_WEIGHT;
	}

	public void setROW_WEIGHT(BigDecimal rOW_WEIGHT) {
		ROW_WEIGHT = rOW_WEIGHT;
	}

	public BigDecimal getBASE_TAX_BEFORE_DISCOUNT() {
		return BASE_TAX_BEFORE_DISCOUNT;
	}

	public void setBASE_TAX_BEFORE_DISCOUNT(BigDecimal bASE_TAX_BEFORE_DISCOUNT) {
		BASE_TAX_BEFORE_DISCOUNT = bASE_TAX_BEFORE_DISCOUNT;
	}

	public BigDecimal getTAX_BEFORE_DISCOUNT() {
		return TAX_BEFORE_DISCOUNT;
	}

	public void setTAX_BEFORE_DISCOUNT(BigDecimal tAX_BEFORE_DISCOUNT) {
		TAX_BEFORE_DISCOUNT = tAX_BEFORE_DISCOUNT;
	}

	public String getEXT_ORDER_ITEM_ID() {
		return EXT_ORDER_ITEM_ID;
	}

	public void setEXT_ORDER_ITEM_ID(String eXT_ORDER_ITEM_ID) {
		EXT_ORDER_ITEM_ID = eXT_ORDER_ITEM_ID;
	}

	public Integer getLOCKED_DO_INVOICE() {
		return LOCKED_DO_INVOICE;
	}

	public void setLOCKED_DO_INVOICE(Integer lOCKED_DO_INVOICE) {
		LOCKED_DO_INVOICE = lOCKED_DO_INVOICE;
	}

	public Integer getLOCKED_DO_SHIP() {
		return LOCKED_DO_SHIP;
	}

	public void setLOCKED_DO_SHIP(Integer lOCKED_DO_SHIP) {
		LOCKED_DO_SHIP = lOCKED_DO_SHIP;
	}

	public BigDecimal getPRICE_INCL_TAX() {
		return PRICE_INCL_TAX;
	}

	public void setPRICE_INCL_TAX(BigDecimal pRICE_INCL_TAX) {
		PRICE_INCL_TAX = pRICE_INCL_TAX;
	}

	public BigDecimal getBASE_PRICE_INCL_TAX() {
		return BASE_PRICE_INCL_TAX;
	}

	public void setBASE_PRICE_INCL_TAX(BigDecimal bASE_PRICE_INCL_TAX) {
		BASE_PRICE_INCL_TAX = bASE_PRICE_INCL_TAX;
	}

	public BigDecimal getROW_TOTAL_INCL_TAX() {
		return ROW_TOTAL_INCL_TAX;
	}

	public void setROW_TOTAL_INCL_TAX(BigDecimal rOW_TOTAL_INCL_TAX) {
		ROW_TOTAL_INCL_TAX = rOW_TOTAL_INCL_TAX;
	}

	public BigDecimal getBASE_ROW_TOTAL_INCL_TAX() {
		return BASE_ROW_TOTAL_INCL_TAX;
	}

	public void setBASE_ROW_TOTAL_INCL_TAX(BigDecimal bASE_ROW_TOTAL_INCL_TAX) {
		BASE_ROW_TOTAL_INCL_TAX = bASE_ROW_TOTAL_INCL_TAX;
	}

	public BigDecimal getHIDDEN_TAX_AMOUNT() {
		return HIDDEN_TAX_AMOUNT;
	}

	public void setHIDDEN_TAX_AMOUNT(BigDecimal hIDDEN_TAX_AMOUNT) {
		HIDDEN_TAX_AMOUNT = hIDDEN_TAX_AMOUNT;
	}

	public BigDecimal getBASE_HIDDEN_TAX_AMOUNT() {
		return BASE_HIDDEN_TAX_AMOUNT;
	}

	public void setBASE_HIDDEN_TAX_AMOUNT(BigDecimal bASE_HIDDEN_TAX_AMOUNT) {
		BASE_HIDDEN_TAX_AMOUNT = bASE_HIDDEN_TAX_AMOUNT;
	}

	public BigDecimal getHIDDEN_TAX_INVOICED() {
		return HIDDEN_TAX_INVOICED;
	}

	public void setHIDDEN_TAX_INVOICED(BigDecimal hIDDEN_TAX_INVOICED) {
		HIDDEN_TAX_INVOICED = hIDDEN_TAX_INVOICED;
	}

	public BigDecimal getBASE_HIDDEN_TAX_INVOICED() {
		return BASE_HIDDEN_TAX_INVOICED;
	}

	public void setBASE_HIDDEN_TAX_INVOICED(BigDecimal bASE_HIDDEN_TAX_INVOICED) {
		BASE_HIDDEN_TAX_INVOICED = bASE_HIDDEN_TAX_INVOICED;
	}

	public BigDecimal getHIDDEN_TAX_REFUNDED() {
		return HIDDEN_TAX_REFUNDED;
	}

	public void setHIDDEN_TAX_REFUNDED(BigDecimal hIDDEN_TAX_REFUNDED) {
		HIDDEN_TAX_REFUNDED = hIDDEN_TAX_REFUNDED;
	}

	public BigDecimal getBASE_HIDDEN_TAX_REFUNDED() {
		return BASE_HIDDEN_TAX_REFUNDED;
	}

	public void setBASE_HIDDEN_TAX_REFUNDED(BigDecimal bASE_HIDDEN_TAX_REFUNDED) {
		BASE_HIDDEN_TAX_REFUNDED = bASE_HIDDEN_TAX_REFUNDED;
	}

	public Integer getIS_NOMINAL() {
		return IS_NOMINAL;
	}

	public void setIS_NOMINAL(Integer iS_NOMINAL) {
		IS_NOMINAL = iS_NOMINAL;
	}

	public BigDecimal getTAX_CANCELED() {
		return TAX_CANCELED;
	}

	public void setTAX_CANCELED(BigDecimal tAX_CANCELED) {
		TAX_CANCELED = tAX_CANCELED;
	}

	public BigDecimal getHIDDEN_TAX_CANCELED() {
		return HIDDEN_TAX_CANCELED;
	}

	public void setHIDDEN_TAX_CANCELED(BigDecimal hIDDEN_TAX_CANCELED) {
		HIDDEN_TAX_CANCELED = hIDDEN_TAX_CANCELED;
	}

	public BigDecimal getTAX_REFUNDED() {
		return TAX_REFUNDED;
	}

	public void setTAX_REFUNDED(BigDecimal tAX_REFUNDED) {
		TAX_REFUNDED = tAX_REFUNDED;
	}

	public BigDecimal getBASE_TAX_REFUNDED() {
		return BASE_TAX_REFUNDED;
	}

	public void setBASE_TAX_REFUNDED(BigDecimal bASE_TAX_REFUNDED) {
		BASE_TAX_REFUNDED = bASE_TAX_REFUNDED;
	}

	public BigDecimal getDISCOUNT_REFUNDED() {
		return DISCOUNT_REFUNDED;
	}

	public void setDISCOUNT_REFUNDED(BigDecimal dISCOUNT_REFUNDED) {
		DISCOUNT_REFUNDED = dISCOUNT_REFUNDED;
	}

	public BigDecimal getBASE_DISCOUNT_REFUNDED() {
		return BASE_DISCOUNT_REFUNDED;
	}

	public void setBASE_DISCOUNT_REFUNDED(BigDecimal bASE_DISCOUNT_REFUNDED) {
		BASE_DISCOUNT_REFUNDED = bASE_DISCOUNT_REFUNDED;
	}

	public Integer getGIFT_MESSAGE_ID() {
		return GIFT_MESSAGE_ID;
	}

	public void setGIFT_MESSAGE_ID(Integer gIFT_MESSAGE_ID) {
		GIFT_MESSAGE_ID = gIFT_MESSAGE_ID;
	}

	public Integer getGIFT_MESSAGE_AVAILABLE() {
		return GIFT_MESSAGE_AVAILABLE;
	}

	public void setGIFT_MESSAGE_AVAILABLE(Integer gIFT_MESSAGE_AVAILABLE) {
		GIFT_MESSAGE_AVAILABLE = gIFT_MESSAGE_AVAILABLE;
	}

	public String getWEEE_TAX_APPLIED() {
		return WEEE_TAX_APPLIED;
	}

	public void setWEEE_TAX_APPLIED(String wEEE_TAX_APPLIED) {
		WEEE_TAX_APPLIED = wEEE_TAX_APPLIED;
	}

	public BigDecimal getWEEE_TAX_APPLIED_AMOUNT() {
		return WEEE_TAX_APPLIED_AMOUNT;
	}

	public void setWEEE_TAX_APPLIED_AMOUNT(BigDecimal wEEE_TAX_APPLIED_AMOUNT) {
		WEEE_TAX_APPLIED_AMOUNT = wEEE_TAX_APPLIED_AMOUNT;
	}

	public BigDecimal getWEEE_TAX_APPLIED_ROW_AMOUNT() {
		return WEEE_TAX_APPLIED_ROW_AMOUNT;
	}

	public void setWEEE_TAX_APPLIED_ROW_AMOUNT(
			BigDecimal wEEE_TAX_APPLIED_ROW_AMOUNT) {
		WEEE_TAX_APPLIED_ROW_AMOUNT = wEEE_TAX_APPLIED_ROW_AMOUNT;
	}

	public BigDecimal getBASE_WEEE_TAX_APPLIED_AMOUNT() {
		return BASE_WEEE_TAX_APPLIED_AMOUNT;
	}

	public void setBASE_WEEE_TAX_APPLIED_AMOUNT(
			BigDecimal bASE_WEEE_TAX_APPLIED_AMOUNT) {
		BASE_WEEE_TAX_APPLIED_AMOUNT = bASE_WEEE_TAX_APPLIED_AMOUNT;
	}

	public BigDecimal getBASE_WEEE_TAX_APPLIED_ROW_AMNT() {
		return BASE_WEEE_TAX_APPLIED_ROW_AMNT;
	}

	public void setBASE_WEEE_TAX_APPLIED_ROW_AMNT(
			BigDecimal bASE_WEEE_TAX_APPLIED_ROW_AMNT) {
		BASE_WEEE_TAX_APPLIED_ROW_AMNT = bASE_WEEE_TAX_APPLIED_ROW_AMNT;
	}

	public BigDecimal getWEEE_TAX_DISPOSITION() {
		return WEEE_TAX_DISPOSITION;
	}

	public void setWEEE_TAX_DISPOSITION(BigDecimal wEEE_TAX_DISPOSITION) {
		WEEE_TAX_DISPOSITION = wEEE_TAX_DISPOSITION;
	}

	public BigDecimal getWEEE_TAX_ROW_DISPOSITION() {
		return WEEE_TAX_ROW_DISPOSITION;
	}

	public void setWEEE_TAX_ROW_DISPOSITION(BigDecimal wEEE_TAX_ROW_DISPOSITION) {
		WEEE_TAX_ROW_DISPOSITION = wEEE_TAX_ROW_DISPOSITION;
	}

	public BigDecimal getBASE_WEEE_TAX_DISPOSITION() {
		return BASE_WEEE_TAX_DISPOSITION;
	}

	public void setBASE_WEEE_TAX_DISPOSITION(BigDecimal bASE_WEEE_TAX_DISPOSITION) {
		BASE_WEEE_TAX_DISPOSITION = bASE_WEEE_TAX_DISPOSITION;
	}

	public BigDecimal getBASE_WEEE_TAX_ROW_DISPOSITION() {
		return BASE_WEEE_TAX_ROW_DISPOSITION;
	}

	public void setBASE_WEEE_TAX_ROW_DISPOSITION(
			BigDecimal bASE_WEEE_TAX_ROW_DISPOSITION) {
		BASE_WEEE_TAX_ROW_DISPOSITION = bASE_WEEE_TAX_ROW_DISPOSITION;
	}

	public Integer getEVENT_ID() {
		return EVENT_ID;
	}

	public void setEVENT_ID(Integer eVENT_ID) {
		EVENT_ID = eVENT_ID;
	}

	public Integer getGIFTREGISTRY_ITEM_ID() {
		return GIFTREGISTRY_ITEM_ID;
	}

	public void setGIFTREGISTRY_ITEM_ID(Integer gIFTREGISTRY_ITEM_ID) {
		GIFTREGISTRY_ITEM_ID = gIFTREGISTRY_ITEM_ID;
	}

	public Integer getGW_ID() {
		return GW_ID;
	}

	public void setGW_ID(Integer gW_ID) {
		GW_ID = gW_ID;
	}

	public BigDecimal getGW_BASE_PRICE() {
		return GW_BASE_PRICE;
	}

	public void setGW_BASE_PRICE(BigDecimal gW_BASE_PRICE) {
		GW_BASE_PRICE = gW_BASE_PRICE;
	}

	public BigDecimal getGW_PRICE() {
		return GW_PRICE;
	}

	public void setGW_PRICE(BigDecimal gW_PRICE) {
		GW_PRICE = gW_PRICE;
	}

	public BigDecimal getGW_BASE_TAX_AMOUNT() {
		return GW_BASE_TAX_AMOUNT;
	}

	public void setGW_BASE_TAX_AMOUNT(BigDecimal gW_BASE_TAX_AMOUNT) {
		GW_BASE_TAX_AMOUNT = gW_BASE_TAX_AMOUNT;
	}

	public BigDecimal getGW_TAX_AMOUNT() {
		return GW_TAX_AMOUNT;
	}

	public void setGW_TAX_AMOUNT(BigDecimal gW_TAX_AMOUNT) {
		GW_TAX_AMOUNT = gW_TAX_AMOUNT;
	}

	public BigDecimal getGW_BASE_PRICE_INVOICED() {
		return GW_BASE_PRICE_INVOICED;
	}

	public void setGW_BASE_PRICE_INVOICED(BigDecimal gW_BASE_PRICE_INVOICED) {
		GW_BASE_PRICE_INVOICED = gW_BASE_PRICE_INVOICED;
	}

	public BigDecimal getGW_PRICE_INVOICED() {
		return GW_PRICE_INVOICED;
	}

	public void setGW_PRICE_INVOICED(BigDecimal gW_PRICE_INVOICED) {
		GW_PRICE_INVOICED = gW_PRICE_INVOICED;
	}

	public BigDecimal getGW_BASE_TAX_AMOUNT_INVOICED() {
		return GW_BASE_TAX_AMOUNT_INVOICED;
	}

	public void setGW_BASE_TAX_AMOUNT_INVOICED(
			BigDecimal gW_BASE_TAX_AMOUNT_INVOICED) {
		GW_BASE_TAX_AMOUNT_INVOICED = gW_BASE_TAX_AMOUNT_INVOICED;
	}

	public BigDecimal getGW_TAX_AMOUNT_INVOICED() {
		return GW_TAX_AMOUNT_INVOICED;
	}

	public void setGW_TAX_AMOUNT_INVOICED(BigDecimal gW_TAX_AMOUNT_INVOICED) {
		GW_TAX_AMOUNT_INVOICED = gW_TAX_AMOUNT_INVOICED;
	}

	public BigDecimal getGW_BASE_PRICE_REFUNDED() {
		return GW_BASE_PRICE_REFUNDED;
	}

	public void setGW_BASE_PRICE_REFUNDED(BigDecimal gW_BASE_PRICE_REFUNDED) {
		GW_BASE_PRICE_REFUNDED = gW_BASE_PRICE_REFUNDED;
	}

	public BigDecimal getGW_PRICE_REFUNDED() {
		return GW_PRICE_REFUNDED;
	}

	public void setGW_PRICE_REFUNDED(BigDecimal gW_PRICE_REFUNDED) {
		GW_PRICE_REFUNDED = gW_PRICE_REFUNDED;
	}

	public BigDecimal getGW_BASE_TAX_AMOUNT_REFUNDED() {
		return GW_BASE_TAX_AMOUNT_REFUNDED;
	}

	public void setGW_BASE_TAX_AMOUNT_REFUNDED(
			BigDecimal gW_BASE_TAX_AMOUNT_REFUNDED) {
		GW_BASE_TAX_AMOUNT_REFUNDED = gW_BASE_TAX_AMOUNT_REFUNDED;
	}

	public BigDecimal getGW_TAX_AMOUNT_REFUNDED() {
		return GW_TAX_AMOUNT_REFUNDED;
	}

	public void setGW_TAX_AMOUNT_REFUNDED(BigDecimal gW_TAX_AMOUNT_REFUNDED) {
		GW_TAX_AMOUNT_REFUNDED = gW_TAX_AMOUNT_REFUNDED;
	}

	public BigDecimal getQTY_RETURNED() {
		return QTY_RETURNED;
	}

	public void setQTY_RETURNED(BigDecimal qTY_RETURNED) {
		QTY_RETURNED = qTY_RETURNED;
	}
	
	
}
