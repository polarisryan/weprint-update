package com.avery.staging.dao;

import java.sql.Types;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.avery.staging.model.SalesFlatOrderItem;

@Repository
public class  SalesFlatOrderItemDao {
	   
	@Autowired
	private DataSource datasource;
	
	private JdbcTemplate template;
	
	private static final Logger logger = Logger.getLogger(SalesFlatOrderItemDao.class);
	
	@PostConstruct
	public void init(){
		template = new JdbcTemplate(datasource);
	}
	
	public void updateRecord(SalesFlatOrderItem orderItem) {
	    String sqlUpdate = " update stg_sales_flat_order_item set " +
						   "    PRICE = ?,     " +
				     	   "    BASE_PRICE  = ?,        " +
				     	   "    ORIGINAL_PRICE = ?,          " +
				     	   "    BASE_ORIGINAL_PRICE = ?,            " +
				     	   "    TAX_PERCENT = ?,          " +
				     	   "    TAX_AMOUNT  = ?,             " +
				     	   "    BASE_TAX_AMOUNT = ?,          " +
				     	   "    TAX_INVOICED = ?,          " +
				     	   "    DISCOUNT_PERCENT       = ?,          " +
				     	   "    DISCOUNT_AMOUNT = ?, " +
				     	   "    BASE_DISCOUNT_AMOUNT        = ?, " +
				     	   "    ROW_TOTAL                  = ?, " + 
				     	   "    BASE_ROW_TOTAL             = ?, " +
				     	   "    ROW_INVOICED               = ?, " +
				     	   "    BASE_ROW_INVOICED          = ?, " +
				     	   "    PRICE_INCL_TAX             = ?, " +
				     	   "    BASE_PRICE_INCL_TAX        = ?, " + 
				     	   "    ROW_TOTAL_INCL_TAX          = ?, " +
				     	   "    BASE_ROW_TOTAL_INCL_TAX     = ? " +
				     	   "  where ITEM_ID = " + orderItem.getITEM_ID();
				     		   
        // query arguments
        Object[] params = {
        		           orderItem.getPRICE(),
        		           orderItem.getBASE_PRICE(),
        		           orderItem.getORIGINAL_PRICE(),
        		           orderItem.getBASE_ORIGINAL_PRICE(),
        		           orderItem.getTAX_PERCENT(),
        		           orderItem.getTAX_AMOUNT(),
        		           orderItem.getBASE_TAX_AMOUNT(),
        		           orderItem.getTAX_INVOICED(),
        		           orderItem.getDISCOUNT_PERCENT(),
        		           orderItem.getDISCOUNT_AMOUNT(),
        		           orderItem.getBASE_DISCOUNT_AMOUNT(),
        		           orderItem.getROW_TOTAL(),
        		           orderItem.getBASE_ROW_TOTAL(),
        		           orderItem.getROW_INVOICED(),
        		           orderItem.getBASE_ROW_INVOICED(),
        		           orderItem.getPRICE_INCL_TAX(),
        		           orderItem.getBASE_PRICE_INCL_TAX(),
        		           orderItem.getROW_TOTAL_INCL_TAX(),
        		           orderItem.getBASE_ROW_TOTAL_INCL_TAX()
		                  }; 
        
		 // sql types  
		 int[] types =  { 
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC,
		                  Types.NUMERIC
                         }; 
		
		 int rows = template.update(sqlUpdate, params, types);
		     
		 logger.info("No of sales order item updated: " + rows + " row(s) updated.");
	}
}

