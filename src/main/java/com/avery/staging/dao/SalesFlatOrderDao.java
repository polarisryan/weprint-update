package com.avery.staging.dao;

import java.sql.Types;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.avery.staging.model.SalesFlatOrder;

@Repository
public class  SalesFlatOrderDao {
	   
	@Autowired
	private DataSource datasource;
	
	private JdbcTemplate template;
	
	private static final Logger logger = Logger.getLogger(SalesFlatOrderDao.class);
	
	@PostConstruct
	public void init(){
		template = new JdbcTemplate(datasource);
	}
	
	public void updateRecord(SalesFlatOrder order) {
	    String sqlUpdate = " update stg_sales_flat_order set    " +
					       "	BASE_DISCOUNT_AMOUNT     = ?,   " +
					       "	BASE_DISCOUNT_INVOICED   = ?,   " +
					       "    BASE_GRAND_TOTAL         = ?,   " +
					       "	BASE_SHIPPING_AMOUNT     = ?,   " +
					       "	BASE_SHIPPING_INVOICED   = ?,   " +
					       "	BASE_SHIPPING_TAX_AMOUNT = ?,   " +
					       "	BASE_SUBTOTAL            = ?,   " +
					       "	BASE_SUBTOTAL_INVOICED   = ?,   " +
					       "	BASE_TAX_AMOUNT          = ?,   " +
					       "	BASE_TAX_INVOICED        = ?,   " +
					       "	BASE_TOTAL_INVOICED      = ?,   " +
					       "	BASE_TOTAL_PAID          = ?,   " +
					       "	DISCOUNT_AMOUNT          = ?,   " +
					       "	DISCOUNT_INVOICED        = ?,   " +
					       "	GRAND_TOTAL              = ?,   " +
					       "  	SHIPPING_AMOUNT          = ?,   " +
					       "	SHIPPING_INVOICED        = ?,   " +
					       "	SHIPPING_TAX_AMOUNT      = ?,   " +
					       "	SUBTOTAL                 = ?,   " +
					       "	SUBTOTAL_INVOICED        = ?,   " +
					       "    TAX_AMOUNT               = ?,   " +
					       "	TAX_INVOICED             = ?,   " +
					       "	TOTAL_INVOICED           = ?,   " +
					       "	TOTAL_PAID               = ?,   " +
					       "	BASE_SHIPPING_DISCOUNT_AMOUNT   = ?, " + 
					       "	BASE_SUBTOTAL_INCL_TAX    = ?,   " + 
					       "	SHIPPING_DISCOUNT_AMOUNT  = ?,   " + 
					       "	SUBTOTAL_INCL_TAX         = ?,   " +
					       "	TOTAL_DUE                 = ?,   " +
					       "	SHIPPING_INCL_TAX         = ?,   " +
					       "	BASE_SHIPPING_INCL_TAX    = ?    " +
	    		
				     	   "  where ENTITY_ID = " + "\'" + order.getENTITY_ID() + "\'";
				     		   
        // query arguments
        Object[] params = {
			        		 order.getBASE_DISCOUNT_AMOUNT(), 
			        		 order.getBASE_DISCOUNT_INVOICED(),
			        		 order.getBASE_GRAND_TOTAL(),
			        		 order.getBASE_SHIPPING_AMOUNT(),
			        		 order.getBASE_SHIPPING_INVOICED(),
			        		 order.getBASE_SHIPPING_TAX_AMOUNT(),
			        		 order.getBASE_SUBTOTAL(),
			        		 order.getBASE_SUBTOTAL_INVOICED(),
			        		 order.getBASE_TAX_AMOUNT(),
			        		 order.getBASE_TAX_INVOICED(),
			        		 order.getBASE_TOTAL_INVOICED(),
			        		 order.getBASE_TOTAL_PAID(),
			        		 order.getDISCOUNT_AMOUNT(),
			        		 order.getDISCOUNT_INVOICED(),
			        		 order.getGRAND_TOTAL(),
			        		 order.getSHIPPING_AMOUNT(),
			        		 order.getSHIPPING_INVOICED(),
			        		 order.getSHIPPING_TAX_AMOUNT(),
			        		 order.getSUBTOTAL(),
			        		 order.getSUBTOTAL_INVOICED(),
			        		 order.getTAX_AMOUNT(),
			        		 order.getTAX_INVOICED(),
			        		 order.getTOTAL_INVOICED(),
			        		 order.getTOTAL_PAID(),
			        		 order.getBASE_SHIPPING_DISCOUNT_AMOUNT(),
			        		 order.getBASE_SUBTOTAL_INCL_TAX(),
			        		 order.getSHIPPING_DISCOUNT_AMOUNT(),
			        		 order.getSUBTOTAL_INCL_TAX(),
			        		 order.getTOTAL_DUE(),
			        		 order.getSHIPPING_INCL_TAX(),
			        		 order.getBASE_SHIPPING_INCL_TAX()
		                  }; 
        
		 // sql types  
		 int[] types =  { 
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC,
				 			 Types.NUMERIC
                         }; 
		
		 int rows = template.update(sqlUpdate, params, types);
		     
		 logger.info("No of Sales Flat Order update: " + rows + " row(s) updated.");
	}
}

