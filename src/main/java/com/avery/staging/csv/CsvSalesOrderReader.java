package com.avery.staging.csv;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseBigDecimal;
import org.supercsv.cellprocessor.ParseDate;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.avery.staging.bill.Updater;
import com.avery.staging.model.SalesFlatOrder;

public class CsvSalesOrderReader {	
	
     private static final Logger logger = Logger.getLogger(CsvSalesOrderReader.class);
	
	 public String[] header = { 
			    "ENTITY_ID",     
				"STATE",   
				"STATUS",
				"COUPON_CODE",  
				"PROTECT_CODE",   
				"SHIPPING_DESCRIPTION",             
				"IS_VIRTUAL",      
				"STORE_ID",   
				"CUSTOMER_ID",           
				"BASE_DISCOUNT_AMOUNT",        
				"BASE_DISCOUNT_CANCELED", 
				"BASE_DISCOUNT_INVOICED",   
				"BASE_DISCOUNT_REFUNDED",   
				"BASE_GRAND_TOTAL",   
				"BASE_SHIPPING_AMOUNT",   
				"BASE_SHIPPING_CANCELED",   
				"BASE_SHIPPING_INVOICED",   
				"BASE_SHIPPING_REFUNDED",   
				"BASE_SHIPPING_TAX_AMOUNT",   
				"BASE_SHIPPING_TAX_REFUNDED", 
				"BASE_SUBTOTAL",   
				"BASE_SUBTOTAL_CANCELED",   
				"BASE_SUBTOTAL_INVOICED",   
				"BASE_SUBTOTAL_REFUNDED",   
				"BASE_TAX_AMOUNT",   
				"BASE_TAX_CANCELED",   
				"BASE_TAX_INVOICED",
				"BASE_TAX_REFUNDED",
				"BASE_TO_GLOBAL_RATE",
				"BASE_TO_ORDER_RATE",
				"BASE_TOTAL_CANCELED",
				"BASE_TOTAL_INVOICED", 
				"BASE_TOTAL_INVOICED_COST",
				"BASE_TOTAL_OFFLINE_REFUNDED",
				"BASE_TOTAL_ONLINE_REFUNDED",
				"BASE_TOTAL_PAID",
				"BASE_TOTAL_QTY_ORDERED",
				"BASE_TOTAL_REFUNDED",
				"DISCOUNT_AMOUNT",
				"DISCOUNT_CANCELED",
				"DISCOUNT_INVOICED", 
				"DISCOUNT_REFUNDED", 
				"GRAND_TOTAL",   
				"SHIPPING_AMOUNT",  
				"SHIPPING_CANCELED",
				"SHIPPING_INVOICED", 
				"SHIPPING_REFUNDED",   
				"SHIPPING_TAX_AMOUNT",
				"SHIPPING_TAX_REFUNDED",
				"STORE_TO_BASE_RATE",
				"STORE_TO_ORDER_RATE",
				"SUBTOTAL",
				"SUBTOTAL_CANCELED",
				"SUBTOTAL_INVOICED",
				"SUBTOTAL_REFUNDED",
				"TAX_AMOUNT",   
				"TAX_CANCELED",   
				"TAX_INVOICED",   
				"TAX_REFUNDED",   
				"TOTAL_CANCELED",
				"TOTAL_INVOICED",
				"TOTAL_OFFLINE_REFUNDED",
				"TOTAL_ONLINE_REFUNDED",
				"TOTAL_PAID",
				"TOTAL_QTY_ORDERED", 
				"TOTAL_REFUNDED", 
				"CAN_SHIP_PARTIALLY",      
				"CAN_SHIP_PARTIALLY_ITEM",      
				"CUSTOMER_IS_GUEST",      
				"CUSTOMER_NOTE_NOTIFY",      
				"BILLING_ADDRESS_ID",     
				"CUSTOMER_GROUP_ID",      
				"EDIT_INCREMENT",     
				"EMAIL_SENT",     
				"FORCED_SHIPMENT_WITH_INVOICE",      
				"PAYMENT_AUTH_EXPIRATION",     
				"QUOTE_ADDRESS_ID",     
				"QUOTE_ID",     
				"SHIPPING_ADDRESS_ID",     
				"ADJUSTMENT_NEGATIVE",  
				"ADJUSTMENT_POSITIVE",   
				"BASE_ADJUSTMENT_NEGATIVE",   
				"BASE_ADJUSTMENT_POSITIVE",   
				"BASE_SHIPPING_DISCOUNT_AMOUNT",   
				"BASE_SUBTOTAL_INCL_TAX",   
				"BASE_TOTAL_DUE",   
				"PAYMENT_AUTHORIZATION_AMOUNT",   
				"SHIPPING_DISCOUNT_AMOUNT",   
				"SUBTOTAL_INCL_TAX",   
				"TOTAL_DUE",   
				"WEIGHT",   
				"CUSTOMER_DOB",   
				"INCREMENT_ID",  
				"APPLIED_RULE_IDS",
				"BASE_CURRENCY_CODE",
				"CUSTOMER_EMAIL",
				"CUSTOMER_FIRSTNAME",
				"CUSTOMER_LASTNAME",  
				"CUSTOMER_MIDDLENAME",  
				"CUSTOMER_PREFIX",  
				"CUSTOMER_SUFFIX",  
				"CUSTOMER_TAXVAT",  
				"DISCOUNT_DESCRIPTION",  
				"EXT_CUSTOMER_ID",  
				"EXT_ORDER_ID",  
				"GLOBAL_CURRENCY_CODE",    
				"HOLD_BEFORE_STATE",  
				"HOLD_BEFORE_STATUS",  
				"ORDER_CURRENCY_CODE",  
				"ORIGINAL_INCREMENT_ID",   
				"RELATION_CHILD_ID",  
				"RELATION_CHILD_REAL_ID",   
				"RELATION_PARENT_ID",   
				"RELATION_PARENT_REAL_ID",   
				"REMOTE_IP",  
				"SHIPPING_METHOD",  
				"STORE_CURRENCY_CODE",    
				"STORE_NAME",  
				"X_FORWARDED_FOR",  
				"CUSTOMER_NOTE", 
				"CREATED_AT",   
				"UPDATED_AT",  
				"TOTAL_ITEM_COUNT",    
				"CUSTOMER_GENDER",    
				"HIDDEN_TAX_AMOUNT",
				"BASE_HIDDEN_TAX_AMOUNT",   
				"SHIPPING_HIDDEN_TAX_AMOUNT",   
				"BASE_SHIPPING_HIDDEN_TAX_AMNT",   
				"HIDDEN_TAX_INVOICED",   
				"BASE_HIDDEN_TAX_INVOICED",
				"HIDDEN_TAX_REFUNDED",   
				"BASE_HIDDEN_TAX_REFUNDED",   
				"SHIPPING_INCL_TAX",   
				"BASE_SHIPPING_INCL_TAX",   
				"COUPON_RULE_NAME", 
				"PAYPAL_IPN_CUSTOMER_NOTIFIED",
				"GIFT_MESSAGE_ID", 
				"BASE_CUSTOMER_BALANCE_AMOUNT",
				"CUSTOMER_BALANCE_AMOUNT",
				"BASE_CUSTOMER_BALANCE_INVOICED", 
				"CUSTOMER_BALANCE_INVOICED",
				"BASE_CUSTOMER_BALANCE_REFUNDED", 
				"CUSTOMER_BALANCE_REFUNDED",
				"BS_CUSTOMER_BAL_TOTAL_REFUNDED",
				"CUSTOMER_BAL_TOTAL_REFUNDED",
				"GIFT_CARDS",
				"BASE_GIFT_CARDS_AMOUNT",   
				"GIFT_CARDS_AMOUNT",   
				"BASE_GIFT_CARDS_INVOICED",   
				"GIFT_CARDS_INVOICED",   
				"BASE_GIFT_CARDS_REFUNDED",   
				"GIFT_CARDS_REFUNDED",  
				"GW_ID",     
				"GW_ALLOW_GIFT_RECEIPT",     
				"GW_ADD_CARD",  
				"GW_BASE_PRICE",   
				"GW_PRICE",   
				"GW_ITEMS_BASE_PRICE",   
				"GW_ITEMS_PRICE",   
				"GW_CARD_BASE_PRICE",   
				"GW_CARD_PRICE",   
				"GW_BASE_TAX_AMOUNT",   
				"GW_TAX_AMOUNT",   
				"GW_ITEMS_BASE_TAX_AMOUNT",   
				"GW_ITEMS_TAX_AMOUNT",   
				"GW_CARD_BASE_TAX_AMOUNT",   
				"GW_CARD_TAX_AMOUNT",   
				"GW_BASE_PRICE_INVOICED",   
				"GW_PRICE_INVOICED",   
				"GW_ITEMS_BASE_PRICE_INVOICED",   
				"GW_ITEMS_PRICE_INVOICED",   
				"GW_CARD_BASE_PRICE_INVOICED",   
				"GW_CARD_PRICE_INVOICED",   
				"GW_BASE_TAX_AMOUNT_INVOICED",   
				"GW_TAX_AMOUNT_INVOICED",   
				"GW_ITEMS_BASE_TAX_INVOICED",   
				"GW_ITEMS_TAX_INVOICED",   
				"GW_CARD_BASE_TAX_INVOICED",   
				"GW_CARD_TAX_INVOICED",   
				"GW_BASE_PRICE_REFUNDED",   
				"GW_PRICE_REFUNDED",   
				"GW_ITEMS_BASE_PRICE_REFUNDED",   
				"GW_ITEMS_PRICE_REFUNDED",   
				"GW_CARD_BASE_PRICE_REFUNDED",   
				"GW_CARD_PRICE_REFUNDED",   
				"GW_BASE_TAX_AMOUNT_REFUNDED",   
				"GW_TAX_AMOUNT_REFUNDED",   
				"GW_ITEMS_BASE_TAX_REFUNDED",   
				"GW_ITEMS_TAX_REFUNDED",   
				"GW_CARD_BASE_TAX_REFUNDED",   
				"GW_CARD_TAX_REFUNDED", 
				"REWARD_POINTS_BALANCE",     
				"BASE_REWARD_CURRENCY_AMOUNT",   
				"REWARD_CURRENCY_AMOUNT",   
				"BASE_RWRD_CRRNCY_AMT_INVOICED",   
				"RWRD_CURRENCY_AMOUNT_INVOICED",   
				"BASE_RWRD_CRRNCY_AMNT_REFNDED",   
				"RWRD_CRRNCY_AMNT_REFUNDED",  
				"REWARD_POINTS_BALANCE_REFUND",     
				"REWARD_POINTS_BALANCE_REFUNDED",     
				"REWARD_SALESRULE_POINTS",     
				"PRESSWISE_ORDER_ID", 
				"TRACK_NUMBER",
				"USPS_TRACK_NUMBER",
				"SHIP_DATE", 
				"CREATED_AT_PST",
				"UPDATED_AT_PST",   
				"CUSTOMER_TYPE",   
				"UPS_YELLOWBAG_TRACK_NUMBER",
				"HANDLING_AMOUNT",   
				"BASE_HANDLING_AMOUNT"
	 };
	
	 public List<SalesFlatOrder> readCsv(String csvFileName) throws IOException {

		    List<SalesFlatOrder> list = new ArrayList<SalesFlatOrder>();
	        ICsvBeanReader beanReader = null;
	        
	        try {
	            beanReader = new CsvBeanReader(new FileReader(csvFileName), CsvPreference.STANDARD_PREFERENCE);
	            
	            beanReader.getHeader(true);
	            final CellProcessor[] processors = getCsvProcessors();

	            SalesFlatOrder order;  
	            
	            while ((order = beanReader.read(SalesFlatOrder.class, header, processors)) != null) {
	                
	                logger.info(order);
	                list.add(order);
	            }
	        } catch(Exception e){

	        	e.printStackTrace();
	        } finally{
	            if (beanReader != null) {
	                beanReader.close();
	            }
	        }
	        
	        return list;
	 }
	  
	 private static CellProcessor[] getCsvProcessors(){
	        return new CellProcessor[] {
	        		
	        		new Optional(new ParseInt()),  
	        		new NotNull(),  
	        		new NotNull(),
	        		null,  
	        		new NotNull(),   
	        		new NotNull(),             
	        		new Optional(new ParseInt()),      
	        	    new Optional(new ParseInt()),   
	        	    new Optional(new ParseInt()),           
	        	    new Optional(new ParseBigDecimal()),       
	        	    new Optional(new ParseBigDecimal()), 
	        	    new Optional(new ParseBigDecimal()),   
	        	    new Optional(new ParseBigDecimal()),   
	        	    new Optional(new ParseBigDecimal()),  
	        	    new Optional(new ParseBigDecimal()),  
	        	    new Optional(new ParseBigDecimal()),    
	        	    new Optional(new ParseBigDecimal()),    
                    new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),  
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()),  
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()),  
	        		new Optional(new ParseBigDecimal()),  
	        		new Optional(new ParseBigDecimal()),   
	        		new Optional(new ParseBigDecimal()),   
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()),  
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()),   
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()),  
	        		new Optional(new ParseBigDecimal()),  
	        		new Optional(new ParseInt()),          			// CAN_SHIP_PARTIALLY      
	        		new Optional(new ParseInt()),                     // CAN_SHIP_PARTIALLY_ITEM      
	        		new Optional(new ParseInt()),                     // CUSTOMER_IS_GUEST      
	        		new Optional(new ParseInt()),                     // CUSTOMER_NOTE_NOTIFY      
	        		new Optional(new ParseInt()),                     // BILLING_ADDRESS_ID     
	        		new Optional(new ParseInt()),                     // CUSTOMER_GROUP_ID      
	        		new Optional(new ParseInt()),                     // EDIT_INCREMENT   
	        		new Optional(new ParseInt()),                     // EMAIL_SENT  
	        		new Optional(new ParseInt()),                     // FORCED_SHIPMENT_WITH_INVOICE      
	        		new Optional(new ParseInt()),                     // PAYMENT_AUTH_EXPIRATION     
	        		new Optional(new ParseInt()),                     // QUOTE_ADDRESS_ID
	        		new Optional(new ParseInt()),                     // QUOTE_ID     
	        		new Optional(new ParseInt()),                     // SHIPPING_ADDRESS_ID     
	        		new Optional(new ParseBigDecimal()),              // ADJUSTMENT_NEGATIVE  
	        		new Optional(new ParseBigDecimal()),              // ADJUSTMENT_POSITIVE   
	        		new Optional(new ParseBigDecimal()),              // BASE_ADJUSTMENT_NEGATIVE   
	        		new Optional(new ParseBigDecimal()),              // BASE_ADJUSTMENT_POSITIVE   
	        		new Optional(new ParseBigDecimal()),              // BASE_SHIPPING_DISCOUNT_AMOUNT   
	        		new Optional(new ParseBigDecimal()),              // BASE_SUBTOTAL_INCL_TAX   
	        		new Optional(new ParseBigDecimal()),              // BASE_TOTAL_DUE   
	        		new Optional(new ParseBigDecimal()),              // PAYMENT_AUTHORIZATION_AMOUNT   
	        		new Optional(new ParseBigDecimal()),              // SHIPPING_DISCOUNT_AMOUNT   
	        		new Optional(new ParseBigDecimal()),              // SUBTOTAL_INCL_TAX   
	        		new Optional(new ParseBigDecimal()),              // TOTAL_DUE   
	        		new Optional(new ParseBigDecimal()),              // WEIGHT   
	        		new Optional(new ParseDate("yyyy-mm-dd HH.mm.ss.SSS")),   //CUSTOMER_DOB;   
	        		null,   // INCREMENT_ID  
	        		null,   // APPLIED_RULE_IDS
	        		null,   // BASE_CURRENCY_CODE
	        		null,   // CUSTOMER_EMAIL
	        		null,   // CUSTOMER_FIRSTNAME
	        		null,   // CUSTOMER_LASTNAME  
	        		null,   // CUSTOMER_MIDDLENAME  
	        		null,   // CUSTOMER_PREFIX  
	        		null,   // CUSTOMER_SUFFIX  
	        		null,   // CUSTOMER_TAXVAT  
	        		null,   // DISCOUNT_DESCRIPTION  
	        		null,   // EXT_CUSTOMER_ID  
	        		null,   // EXT_ORDER_ID  
	        		null,   // GLOBAL_CURRENCY_CODE    
	        		null,   // HOLD_BEFORE_STATE  
	        		null,   // HOLD_BEFORE_STATUS  
	        		null,   // ORDER_CURRENCY_CODE  
	        		null,   // ORIGINAL_INCREMENT_ID   
	        		null,   // RELATION_CHILD_ID;  
	        		null,   // RELATION_CHILD_REAL_ID;   
	        		null,   // RELATION_PARENT_ID;   
	        		null,   // RELATION_PARENT_REAL_ID;   
	        		null,   // REMOTE_IP;  
	        		null,   // SHIPPING_METHOD;  
	        		null,   // STORE_CURRENCY_CODE;    
	        		null,   // STORE_NAME;  
	        		null,   // X_FORWARDED_FOR;  
	        		null,   // CUSTOMER_NOTE; 
	        		new Optional(new ParseDate("dd-MMM-yy HH.mm.ss.SSS")),    // CREATED_AT;   
	        		new Optional(new ParseDate("dd-MMM-yy HH.mm.ss.SSS")),    // UPDATED_AT;  
	        		new Optional(new ParseInt()),         // TOTAL_ITEM_COUNT;    
	        		new Optional(new ParseInt()),         // Integer CUSTOMER_GENDER;    
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()),   
	        		new Optional(new ParseBigDecimal()), 
	        		null, 
	        		new Optional(new ParseInt()), 
	        		new Optional(new ParseInt()),  
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()),
	        		new Optional(new ParseBigDecimal()), 
	        		new Optional(new ParseBigDecimal()),
	        		new Optional(new ParseBigDecimal()),
	        		new Optional(new ParseBigDecimal()),
	        		null,
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),   
	        		new Optional(new ParseInt()),    
	        		new Optional(new ParseInt()),      
	        		new Optional(new ParseInt()),   
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),   
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),   
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),    
	        		new Optional(new ParseBigDecimal()),   // GW_CARD_PRICE_REFUNDED   
	        		new Optional(new ParseBigDecimal()),   // GW_BASE_TAX_AMOUNT_REFUNDED   
	        		new Optional(new ParseBigDecimal()),   // GW_TAX_AMOUNT_REFUNDED
	        		new Optional(new ParseBigDecimal()),   // GW_ITEMS_BASE_TAX_REFUNDED   
	        		new Optional(new ParseBigDecimal()),   // GW_ITEMS_TAX_REFUNDED   
	        		new Optional(new ParseBigDecimal()),   // GW_CARD_BASE_TAX_REFUNDED   
	        		new Optional(new ParseBigDecimal()),   // GW_CARD_TAX_REFUNDED
	        		new Optional(new ParseInt()),          // REWARD_POINTS_BALANCE     
	        		new Optional(new ParseBigDecimal()),   // BASE_REWARD_CURRENCY_AMOUNT   
	        		new Optional(new ParseBigDecimal()),   // REWARD_CURRENCY_AMOUNT   
	        		new Optional(new ParseBigDecimal()),   // BASE_RWRD_CRRNCY_AMT_INVOICED   
	        		new Optional(new ParseBigDecimal()),   // RWRD_CURRENCY_AMOUNT_INVOICED   
	        		new Optional(new ParseBigDecimal()),   // BASE_RWRD_CRRNCY_AMNT_REFNDED   
	        		new Optional(new ParseBigDecimal()),   // RWRD_CRRNCY_AMNT_REFUNDED  
	        		new Optional(new ParseInt()),          // REWARD_POINTS_BALANCE_REFUND     
	        		new Optional(new ParseInt()),          // REWARD_POINTS_BALANCE_REFUNDED     
	        		new Optional(new ParseInt()),          // REWARD_SALESRULE_POINTS
	        		   null,                    // PRESSWISE_ORDER_ID
	        		   null,                    // TRACK_NUMBER
	        		   null,                    // USPS_TRACK_NUMBER
	        		new Optional(new ParseDate("dd-MMM-yy HH.mm.ss.SSS")),       // SHIP_DATE 
	        		new Optional(new ParseDate("dd-MMM-yy HH.mm.ss.SSS")),       // CREATED_AT_PST
	        		new Optional(new ParseDate("dd-MMM-yy HH.mm.ss.SSS")),       // UPDATED_AT_PST   
	        		   null,                                           // CUSTOMER_TYPE   
	        		   null,                                           // UPS_YELLOWBAG_TRACK_NUMBER
	        		new Optional(new ParseBigDecimal()),                          // HANDLING_AMOUNT   
	        		new Optional(new ParseBigDecimal())                           // BASE_HANDLING_AMOUNT
	        		};
	 }

}
