package com.avery.staging.csv;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseBigDecimal;
import org.supercsv.cellprocessor.ParseDate;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.avery.staging.bill.Updater;
import com.avery.staging.model.SalesFlatOrder;
import com.avery.staging.model.SalesFlatOrderItem;

public class CsvSalesOrderItemReader {	
	
     private static final Logger logger = Logger.getLogger(CsvSalesOrderReader.class);
	
	 public String[] header = { 
			    "ITEM_ID",     
				"ORDER_ID",     
				"PARENT_ITEM_ID",     
				"QUOTE_ITEM_ID",     
				"STORE_ID",
				"CREATED_AT",
				"UPDATED_AT",
				"PRODUCT_ID",
				"PRODUCT_TYPE",  
				"PRODUCT_OPTIONS", 
				"WEIGHT",   
				"IS_VIRTUAL",     
				"SKU",  
				"NAME",             
				"DESCRIPTION", 
				"APPLIED_RULE_IDS", 
				"ADDITIONAL_DATA", 
				"FREE_SHIPPING",     
				"IS_QTY_DECIMAL",     
				"NO_DISCOUNT",     
				"QTY_BACKORDERED",   
				"QTY_CANCELED",   
				"QTY_INVOICED",                            
				"QTY_ORDERED",                             
				"QTY_REFUNDED",                             
				"QTY_SHIPPED",                             
				"BASE_COST",                                 
				"PRICE",                            
				"BASE_PRICE",                      
				"ORIGINAL_PRICE",                         
				"BASE_ORIGINAL_PRICE",                      
				"TAX_PERCENT",                          
				"TAX_AMOUNT",                               
				"BASE_TAX_AMOUNT",                           
				"TAX_INVOICED",                              
				"BASE_TAX_INVOICED",   
				"DISCOUNT_PERCENT",   
				"DISCOUNT_AMOUNT",   
				"BASE_DISCOUNT_AMOUNT",   
				"DISCOUNT_INVOICED",   
				"BASE_DISCOUNT_INVOICED",   
				"AMOUNT_REFUNDED",
				"BASE_AMOUNT_REFUNDED",   
				"ROW_TOTAL",   
				"BASE_ROW_TOTAL",   
				"ROW_INVOICED",   
				"BASE_ROW_INVOICED",   
				"ROW_WEIGHT",   
				"BASE_TAX_BEFORE_DISCOUNT",
				"TAX_BEFORE_DISCOUNT",   
				"EXT_ORDER_ITEM_ID",  
				"LOCKED_DO_INVOICE",     
				"LOCKED_DO_SHIP",
				"PRICE_INCL_TAX",   
				"BASE_PRICE_INCL_TAX",   
				"ROW_TOTAL_INCL_TAX",   
				"BASE_ROW_TOTAL_INCL_TAX",   
				"HIDDEN_TAX_AMOUNT",   
				"BASE_HIDDEN_TAX_AMOUNT",   
				"HIDDEN_TAX_INVOICED",   
				"BASE_HIDDEN_TAX_INVOICED",   
				"HIDDEN_TAX_REFUNDED",   
				"BASE_HIDDEN_TAX_REFUNDED",   
				"IS_NOMINAL",     
				"TAX_CANCELED",   
				"HIDDEN_TAX_CANCELED",   
				"TAX_REFUNDED",   
				"BASE_TAX_REFUNDED",   
				"DISCOUNT_REFUNDED",   
				"BASE_DISCOUNT_REFUNDED",   
				"GIFT_MESSAGE_ID",     
				"GIFT_MESSAGE_AVAILABLE",     
				"WEEE_TAX_APPLIED", 
				"WEEE_TAX_APPLIED_AMOUNT",   
				"WEEE_TAX_APPLIED_ROW_AMOUNT",   
				"BASE_WEEE_TAX_APPLIED_AMOUNT",   
				"BASE_WEEE_TAX_APPLIED_ROW_AMNT",   
				"WEEE_TAX_DISPOSITION",
				"WEEE_TAX_ROW_DISPOSITION",
				"BASE_WEEE_TAX_DISPOSITION",
				"BASE_WEEE_TAX_ROW_DISPOSITION",   
				"EVENT_ID",     
				"GIFTREGISTRY_ITEM_ID",     
				"GW_ID",     
				"GW_BASE_PRICE",   
				"GW_PRICE",   
				"GW_BASE_TAX_AMOUNT", 
				"GW_TAX_AMOUNT",
				"GW_BASE_PRICE_INVOICED",
				"GW_PRICE_INVOICED",
				"GW_BASE_TAX_AMOUNT_INVOICED",
				"GW_TAX_AMOUNT_INVOICED",
				"GW_BASE_PRICE_REFUNDED",  
				"GW_PRICE_REFUNDED",  
				"GW_BASE_TAX_AMOUNT_REFUNDED",  
				"GW_TAX_AMOUNT_REFUNDED",
				"QTY_RETURNED"
			    
	 };
	
	 public List<SalesFlatOrderItem> readCsv(String csvFileName) throws IOException {

		    List<SalesFlatOrderItem> list = new ArrayList<SalesFlatOrderItem>();
	        ICsvBeanReader beanReader = null;
	        
	        try {
	            beanReader = new CsvBeanReader(new FileReader(csvFileName), CsvPreference.STANDARD_PREFERENCE);
	            
	            beanReader.getHeader(true);
	            final CellProcessor[] processors = getCsvProcessors();

	            SalesFlatOrderItem orderItem;  
	            
	            while ((orderItem = beanReader.read(SalesFlatOrderItem.class, header, processors)) != null) {
	                
	                logger.info(orderItem);
	                list.add(orderItem);
	            }
	        } catch(Exception e){

	        	e.printStackTrace();
	        } finally{
	            if (beanReader != null) {
	                beanReader.close();
	            }
	        }
	        
	        return list;
	 }
	  
	 private static CellProcessor[] getCsvProcessors(){
	        return new CellProcessor[] {
	        		
		            new Optional(new ParseInt()),           // 	 ITEM_ID;     
	        		new Optional(new ParseInt()),           // 	 ORDER_ID;     
	        		new Optional(new ParseInt()),           // 	 PARENT_ITEM_ID;     
	        		new Optional(new ParseInt()),           // 	 QUOTE_ITEM_ID;     
	        		new Optional(new ParseInt()),           // 	 STORE_ID;
	        		new Optional(new ParseDate("dd-MMM-yy HH.mm.ss.SSS")),   //    	 CREATED_AT;
	        		new Optional(new ParseDate("dd-MMM-yy HH.mm.ss.SSS")),   //   	 UPDATED_AT;
	        		new Optional(new ParseInt()),           // 	 PRODUCT_ID;
	        		null,                  //  	 PRODUCT_TYPE;  
	        		null,                  //  	 PRODUCT_OPTIONS; 
	        		new Optional(new ParseBigDecimal()),               //   WEIGHT;   
	        		new Optional(new ParseInt()),           //      IS_VIRTUAL;     
	        		null,                  //       SKU;  
	        		null,                  //       NAME;             
	        		null,                  //       DESCRIPTION; 
	        		null,                  //       APPLIED_RULE_IDS; 
	        		null,                  //       ADDITIONAL_DATA; 
	        		new Optional(new ParseInt()),           //      FREE_SHIPPING;     
	        		new Optional(new ParseInt()),           //      IS_QTY_DECIMAL;     
	        		new Optional(new ParseInt()),           //      NO_DISCOUNT;     
	        		new Optional(new ParseBigDecimal()),               //   QTY_BACKORDERED;   
	        		new Optional(new ParseBigDecimal()),               //   QTY_CANCELED;   
	        		new Optional(new ParseBigDecimal()),               //   QTY_INVOICED;                            
	        		new Optional(new ParseBigDecimal()),               //   QTY_ORDERED;                             
	        		new Optional(new ParseBigDecimal()),               //   QTY_REFUNDED;                             
	        		new Optional(new ParseBigDecimal()),               //   QTY_SHIPPED;                             
	        		new Optional(new ParseBigDecimal()),               //   BASE_COST;                                 
	        		new Optional(new ParseBigDecimal()),               //   PRICE;                            
	        		new Optional(new ParseBigDecimal()),               //   BASE_PRICE;                      
	        		new Optional(new ParseBigDecimal()),               //   ORIGINAL_PRICE;                         
	        		new Optional(new ParseBigDecimal()),               //   BASE_ORIGINAL_PRICE;                      
	        		new Optional(new ParseBigDecimal()),               //   TAX_PERCENT;                          
	        		new Optional(new ParseBigDecimal()),               //   TAX_AMOUNT;                               
	        		new Optional(new ParseBigDecimal()),               //   BASE_TAX_AMOUNT;                           
	        		new Optional(new ParseBigDecimal()),               //   TAX_INVOICED;                              
	        		new Optional(new ParseBigDecimal()),               //   BASE_TAX_INVOICED;   
	        		new Optional(new ParseBigDecimal()),               //   DISCOUNT_PERCENT;   
	        		new Optional(new ParseBigDecimal()),               //   DISCOUNT_AMOUNT;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_DISCOUNT_AMOUNT;   
	        		new Optional(new ParseBigDecimal()),               //   DISCOUNT_INVOICED;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_DISCOUNT_INVOICED;   
	        		new Optional(new ParseBigDecimal()),               //   AMOUNT_REFUNDED;
	        		new Optional(new ParseBigDecimal()),               //   BASE_AMOUNT_REFUNDED;   
	        		new Optional(new ParseBigDecimal()),               //   ROW_TOTAL;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_ROW_TOTAL;   
	        		new Optional(new ParseBigDecimal()),               //   ROW_INVOICED;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_ROW_INVOICED;   
	        		new Optional(new ParseBigDecimal()),               //   ROW_WEIGHT;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_TAX_BEFORE_DISCOUNT;
	        		new Optional(new ParseBigDecimal()),               //   TAX_BEFORE_DISCOUNT;   
	        		null,                                   //       EXT_ORDER_ITEM_ID;  
	        		new Optional(new ParseInt()),           //      LOCKED_DO_INVOICE;     
	        		new Optional(new ParseInt()),           //      LOCKED_DO_SHIP;
	        		new Optional(new ParseBigDecimal()),               //   PRICE_INCL_TAX;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_PRICE_INCL_TAX;   
	        		new Optional(new ParseBigDecimal()),               //   ROW_TOTAL_INCL_TAX;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_ROW_TOTAL_INCL_TAX;   
	        		new Optional(new ParseBigDecimal()),               //   HIDDEN_TAX_AMOUNT;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_HIDDEN_TAX_AMOUNT;   
	        		new Optional(new ParseBigDecimal()),               //   HIDDEN_TAX_INVOICED;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_HIDDEN_TAX_INVOICED;   
	        		new Optional(new ParseBigDecimal()),               //   HIDDEN_TAX_REFUNDED;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_HIDDEN_TAX_REFUNDED;   
	        		new Optional(new ParseInt()),           //      IS_NOMINAL;     
	        		new Optional(new ParseBigDecimal()),               //   TAX_CANCELED;   
	        		new Optional(new ParseBigDecimal()),               //   HIDDEN_TAX_CANCELED;   
	        		new Optional(new ParseBigDecimal()),               //   TAX_REFUNDED;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_TAX_REFUNDED;   
	        		new Optional(new ParseBigDecimal()),               //   DISCOUNT_REFUNDED;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_DISCOUNT_REFUNDED;   
	        		new Optional(new ParseInt()),           //      GIFT_MESSAGE_ID;     
	        		new Optional(new ParseInt()),           //      GIFT_MESSAGE_AVAILABLE;     
	        		null,                                   //       WEEE_TAX_APPLIED; 
	        		new Optional(new ParseBigDecimal()),               //   WEEE_TAX_APPLIED_AMOUNT;   
	        		new Optional(new ParseBigDecimal()),               //   WEEE_TAX_APPLIED_ROW_AMOUNT;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_WEEE_TAX_APPLIED_AMOUNT;   
	        		new Optional(new ParseBigDecimal()),               //   BASE_WEEE_TAX_APPLIED_ROW_AMNT;   
	        		new Optional(new ParseBigDecimal()),               //   WEEE_TAX_DISPOSITION;
	        		new Optional(new ParseBigDecimal()),               //   WEEE_TAX_ROW_DISPOSITION;
	        		new Optional(new ParseBigDecimal()),               //   BASE_WEEE_TAX_DISPOSITION;
	        		new Optional(new ParseBigDecimal()),               //   BASE_WEEE_TAX_ROW_DISPOSITION;   
	        		new Optional(new ParseInt()),           //      EVENT_ID;     
	        		new Optional(new ParseInt()),           //      GIFTREGISTRY_ITEM_ID;     
	        		new Optional(new ParseInt()),           //      GW_ID;     
	        		new Optional(new ParseBigDecimal()),               //   GW_BASE_PRICE;   
	        		new Optional(new ParseBigDecimal()),               //   GW_PRICE;   
	        		new Optional(new ParseBigDecimal()),               //   GW_BASE_TAX_AMOUNT; 
	        		new Optional(new ParseBigDecimal()),               //   GW_TAX_AMOUNT;
	        		new Optional(new ParseBigDecimal()),               //   GW_BASE_PRICE_INVOICED;
	        		new Optional(new ParseBigDecimal()),               //   GW_PRICE_INVOICED;
	        		new Optional(new ParseBigDecimal()),               //   GW_BASE_TAX_AMOUNT_INVOICED;
	        		new Optional(new ParseBigDecimal()),               //   GW_TAX_AMOUNT_INVOICED;
	        		new Optional(new ParseBigDecimal()),               //   GW_BASE_PRICE_REFUNDED;  
	        		new Optional(new ParseBigDecimal()),               //   GW_PRICE_REFUNDED;  
	        		new Optional(new ParseBigDecimal()),               //   GW_BASE_TAX_AMOUNT_REFUNDED;  
	        		new Optional(new ParseBigDecimal()),               //   GW_TAX_AMOUNT_REFUNDED;
	        		new Optional(new ParseBigDecimal())                //   QTY_RETURNED;
	        		
	        		};
	 }

}
